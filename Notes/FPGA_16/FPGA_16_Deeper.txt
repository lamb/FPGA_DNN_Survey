Conference: FPGA '16 (Feb)
Title: Going Deeper with Embedded FPGA Platform for Convolutional Neural Network
Author: Jiantao Qiu
Organization: Tsinghua University

Summary: This a detailed work that discusses in-depth their implementation in FPGA hardware. They focus on reducing memory bandwidth indirectly by reducing the data size by using dynamic precision data quantization. With dynamic-precision quantization, they can use much shorter representations of operations while still achieving comparable accuracy. They also apply a SVD operation to the weight matrix of the first FC layer. This early work is cited by many of the future works in this area.

Reported Throughput:
	Overall: 137 GOPs
	Convolution: 187.8 GOPs

Technologies:

	FPGA: Xilinx Zynq ZC706
	Programming: Compiler developed in Matlab? Xilinx Vivado
	CNNs: VGG16-SVD
	Data: 8/4 bit dynamic precision quantization.

Optimizations:

 - Dynamic precision data quantization
	Only 0.4% accuracy loss reported. The precision is dynamic for different layers and feature map sets, while static in one layer to minimize truncation error of each layer. Compared with 16-bit quantization 8/4-bit quantization halves the storage space for intermediate data and reduces three-fourths memory footprint of CNN models.

 - Apply SVD to the weight matrix of the first FC layer. 

Notes:

 - A compiler is developed on Matlab to automatically generate instructions.

 - They talk a lot about the hardware design. 