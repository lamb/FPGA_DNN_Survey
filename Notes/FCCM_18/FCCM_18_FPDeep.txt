Conference: FCCM '18 (July) - Poster Paper
Title: FPDeep: Acceleration and Load Balancing of CNN Training on FPGA Clusters
Author: Tong Geng
Organization: Boston University

Summary: This poster presents simulated results of CNN execution on an FPGA Cluster. They improve over previous works by employing efficient pipelining and load balancing.

Reported Throughput:
	Overall: 1 TOP per FPGA
	Convolution:

Technologies:

	FPGA: 10 x Xilinx VC709
	Programming: ??
	DNNs: AlexNet
	Data: 16-bit

Optimizations:

 - load balancing

 - intra-layer and inter-layer partitioning, mapping, and pipelining 

 - stochastic rounding



Notes:

 - CNN framework for FPGA clusters (pipeline approach)

 - Training and inference

 - The Mapping Framework partitions a CNN into a number of segments and maps each segment onto an FPGA

 - User provides network configuration and hardware constructions, output is RTL

 - They are implementing the whole system on AWS using FPGAs with direct interconnects. However current results are gathered based on a single FPGA and a cycle-accurate simulator. 

	