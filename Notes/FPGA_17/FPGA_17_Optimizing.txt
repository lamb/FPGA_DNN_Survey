Conference: FPGA '17 (Feb)
Title: Optimizing Loop Operation and Dataflow in FPGA Acceleration of Deep Convolutional Neural Networks
Author: Yufei Ma
Organization: Arizona State

Summary: In this work, they do an in-depth analysis of loop tiling, loop unrolling, and loop interchange. They show systematically how these parameters can affect computing latency, partial sum storage, on-chip buffer accesses, and external memory accesses. They develop several systems of equations to model the relationships between the loop optimizations and performance metrics, and determine optimal values for the loop optimization parameters.

Reported Throughput:
	Overall: 645.25 GOPs
	Convolution:


Technologies:

	FPGA: Intel Arria 10 GX 1150
	Programming: Verilog
	CNNs: VGG16
	Data: 16-bit pixels, 8-bit weights, 30-bit partial sums


Optimizations:

 - Really no new optimizations introduced. They just have a new approach on how to efficiently apply existing loop optimizations.
  

Notes:
 - Their goal is to:
    Minimize memory access and data movement
    Maximize resource utilization

 - Previous works implemented loop unrolling, loop tiling, and loop interchange, but did not systematically study the impact of these techniques on design efficiency and performance.

 - They focus on reducing computing latency, partial sum storage, the number of on-chip buffer accesses, and the number of external memory accesses.

 - Depending on which loops you unroll and your tile size, you may need to store more or fewer partial sums in memory.

 - The amount of times data is reused can be calculated from the unrolling factors.

 - Loop interchange can greatly impact the number of partial sums as well as the resulting data movements and memory access.

 - They do not use batching, but suggest it as a future work.