Conference: FCCM '17 (July)
Title: FP-DNN: An Automated Framework for Mapping Deep Neural Networks on FPGAs with RTL-HLS Hybrid Templates
Author: Yijin Guan
Organization: PKU + UCLA

Summary: This work is a thorough attempt to provide a high-level approach for mapping DNNs to FPGAs. The user provides a description of a DNN (CNNs, RNNs, LSTMs) in Tensorflow, which is then translated to C++ and OpenCL/Verilog for execution on the FPGA. They show that this approach easily allows designers to utilize different types of DNNs without sacrificing performance compared to previously hand-tuned CNN-to-FPGA implementations.

Reported Throughput:
	Overall: 364.4 GOPS
	Convolution:

Technologies:

	FPGA: Catapult (PikesPeak) system with Altera Stratix-V GSMD5
	Programming: Tensorflow -> C++ host and Verilog/OpenCL device
	NNs: CNNs, LSTM-RNNs, and Residual Nets
	Data: 16-bit fixed point

Optimizations:

 - User can specify data quantization

 - communication optimizations in convolutional layers
	Using Chanel-major scheme?

 - communication optimizations in LSTM and FC layers
	batching of input vectors together?

 - communication optimizations in Other layers
      	Batching scheme

Notes:

 - Takes TensorFlow-described DNNs as input, and automatically generates the hardware implementations of FPGA boards with RTL-HLS hybrid templates.

 - First work to implement ResNet-152 on FPGA

 - They formulate the data buffer reuse problem as a graph coloring problem. (Left-edge algorithm).

 - They use RTL for designing a high-performance computation engine, and OpenCL-based HLS for implementing control logic for the RTL part.

 - They divide each layer into a computation-intensive part (MM) and layer-specific part (communication and data arrangement)
	computation intensive: highly optimized MM in Verilog
	layer-specific: 