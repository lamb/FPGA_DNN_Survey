Conference: FPL '17 (Aug)
Title: Accelerating Low Bit-Width Convolutional Neural Networks with Embedded FPGA
Author:	Li Jiao
Organization: Fudan University


Summary: The authors adopt an almost-binary  approach to classifying images, targeted for embedded devices. They use the DoReFa-Net CNNs with 1-bit weights but 2-bit activations to gain significantly higher accuracy compared to pure BNNs, while still using fewer resources than quantized or full-precision approaches.

Reported Throughput:
	Overall: 106 fps
	Convolution:

Technologies:

	FPGA: Zynq XC7Z0202
	Programming: Vivado 2016
	CNNs: DoReFa-Net on ImageNet
	Data: 

Optimizations:

 

Notes:

 - They think DoReFa-Net is more suitable for embedded applications than BNNs
	higher accuracy while having similar requirement on resources (for ImageNet anyways)

 - DoReFa-Net has 1-bit weights, 2-bit activations

 - Is this similar to a ternary CNN?