Conference: FPGA '15 (Feb)
Title: Optimizing FPGA-based Accelerator Design for Deep Convolutional Neural Networks
Author: Chen Zhang
Organization: Peking + UCLA

Summary: 
  This is an "early" work in this area that uses a roofline model and Computation to Communication (CTC) ratios to explore the design space with a goal of maximizing computation while minimizing bandwidth. The use loop unrolling, pipelining, and tiling and local memory promotion optimizations, and rely on polyhedral-based optimization methods. Their implementation uses AlexNet and Xilinx Vivado on a Xilinx Virtex FPGA. 

Reported Throughput: 
	Overall: 61.62 GOPs

Technologies:

	FPGA: Xilinx Virtex C707, Microblaze
	Programming: Vivado HLS (C and exporting RTL as Vivado's IP core).
	CNNs: AlexNet?
	Data: 32-bit floats

Optimizations:

- Polyhedral based data dependence analysis to derive a series of legal design variants, enumerating loop scheduling and loop tile size.

- Loop unrolling: 
  They separate iterators into Irrelevant, Independent, and Dependent

- Loop pipelining:
  They use a polyhedral-based optimization framework to do the code transformations.

- Loop tiling:
  Instead of applying filters to all images, they tile the input/output image, and apply
  Filters only across one region at a time. This is mandatory to fit a small portion of data on-chip.

- Local Memory Promotion:
  Seems similar to hoisting. Used to reduce redundant operations by promoting accesses to an array to an outer loop. For example instead of loading output feature maps for each input map N, we can iterate over all N before loading/storing.

- Loop Transformations for Data Reuse:
  They use polyhedra-based optimization framework to identify legal loop transformations which somehow enables them to maximize opportunities of data reuse through local memory promotion.


Notes:
- This paper really seems to be the first "modern" attempt at tackling this problem. They
Don't really cite any of the other papers I'm looking at, and most of the papers they cite are from 2010 or so. Also most of the subsequent paper cite this work.

- It seems like there is a high concern across several different works about the balance between memory bandwidth and logic usage, which is closely related to computation demands, on the FPGA.

- They explore the "design space" using the roofline model. They're especially analyzing and optimizing for computing throughput and required memory bandwidth for a specific CNN.

- They only look at convolution, not max pooling or sub-sampling

- FPGA design elements:Es, On-chip buffer, external memory, on/off chip interconnect

-  They use the roofline model to find the computational roof with some equations. This calculates the optimal tile size based on the input parameters. However design variants with the highest computational roof may not have the best performance due to memory bandwidth constraints.

- They provide formulas to compute Computation to Communication Ratios (CTC), based off of above optimizations. 

- They explore the design space, and want to pick the design that is 1) closest to the computation roof, and 2) has the highest CTC ratio, which means it will require the least bandwidth. There may be others close to the computation roof with a lower CTC ratio that would require more bandwidth.

- They show that their method out-performs other methods (2010 - 2013), and outperforms a 16-core Intel CPU (no GPU comparison).

