Conference: FPT '17 (Dec)
Title: FPGA-based ORB Feature Extraction for Real Time Visual SLAM
Author:	Weikang Fang
Organization: Beijing Institute of Technology


Summary:  They implement a SLAM system using ORB (alternative to SIFT) on an FPGA. They perform well compared to ARM and Intel CPU implementations, aiming to provide a balance between performance and energy consumption.

Technologies:

	FPGA: Stratix V
	Programming: 
	CNNs: 
	Data: 

Optimizations:



Notes:
 
 - SLAM - Simulteneous Localization and Mapping
	constructing or updating a map of an unknown environment while simultaneously keeping track of an agent's location within.
	need to accelerate slam for real time
	need to reduce power for battery


- comparison with ARM Krait and Intel Core i5

- ORB Feature extractor, alternative to SIFT
	oFAST + Steered BREIF
