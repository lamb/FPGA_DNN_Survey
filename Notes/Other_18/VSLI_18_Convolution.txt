Conference: VLSI '18 (Apr)
Title: Optimizing the Convolution Operation to Accelerate Deep Neural Networks on FPGA
Author: Yufei Ma
Organization: Arizona State University

Summary: I didn't read most of the paper, but it seems like a direct follow-up of their FPGA '17 Optimizing Loop ... [15] work. They made minor updates, tested two new CNNs, and a new FPGA. Most of the paper is the same discussion (and figures) as [15].

Reported Throughput:
	Overall: 715 GOPs
	Convolution:

Technologies:

	FPGA: Stratix V and Arria 10
	Programming: Verilog
	CNNs: NiN, VGG-16, ResNet-50, ResNet-152
	Data: 16-bit fixed point

Optimizations:

Notes:

 - In this paper, the offset caused by west zero padding is handled by shifting the connection between buffers and register arrays, whereas [15] has to change the storage pattern within one address of input buffer by a padding offset that increases the complexity of transferring data from DRAM to buffers.

 - Compared with [15], the unrolling variables, i.e., Pox × Poy × Pof, of VGG-16 are set to be 7×7×64 on Arria 10 instead of 14 × 14 × 16, where the number of MAC units (= 3136) are the same and both sets of P∗ variables are the common factors of the feature/kernel map sizes resulting in the same computation cycles